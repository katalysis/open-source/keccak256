//
//  sha3ContractSignature.swift
//  KataCLIsm
//
//  Created by Alex Tran-Qui on 07/08/2017.
//  Copyright © 2017 Katalysis / Alex Tran Qui (alex@katalysis.io). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//


import Foundation
//---------------------------------------------------
//SHA3 extension
let Plen = 200
let mult2: UInt64 = 256 * 256
let mult3: UInt64 = 256 * 256 * 256
let mult4: UInt64 = 256 * 256 * 256 * 256
let mult5: UInt64 = 256 * 256 * 256 * 256 * 256
let mult6: UInt64 = 256 * 256 * 256 * 256 * 256 * 256
let mult7: UInt64 = 256 * 256 * 256 * 256 * 256 * 256 * 256
let rho: [UInt8] = [ 1,  3,   6, 10, 15, 21, 28, 36, 45, 55,  2, 14,
                     27, 41, 56,  8, 25, 43, 62, 18, 39, 61, 20, 44]
let pi: [UInt8] = [10,  7, 11, 17, 18, 3, 5, 16,  8, 21, 24, 4,
                   15, 23, 19, 13, 12, 2, 20, 14, 22,  9, 6,  1]
let RC: [UInt64] = [1, 0x8082, 0x800000000000808a, 0x8000000080008000,
                    0x808b, 0x80000001, 0x8000000080008081, 0x8000000000008009,
                    0x8a, 0x88, 0x80008009, 0x8000000a,
                    0x8000808b, 0x800000000000008b, 0x8000000000008089, 0x8000000000008003,
                    0x8000000000008002, 0x8000000000000080, 0x800a, 0x800000008000000a,
                    0x8000000080008081, 0x8000000000008080, 0x80000001, 0x8000000080008008]
/*** Helper macros to unroll the permutation. ***/
func rol(x: UInt64, s: UInt64) -> UInt64{
    return (((x) << s) | ((x) >> (64 - s)))
}
/*** Keccak-f[1600] ***/
func keccakf(aa: inout [UInt8]) {
    var a: [UInt64] = Array(repeating: 0, count:aa.count / 8)
    //var a: [UInt64] = Array(count:aa.count / 8, repeating: 0)
    for i in 0..<a.count {
        a[i] += UInt64(aa[i*8])
        a[i] += UInt64(aa[i*8+1]) * 256
        a[i] += UInt64(aa[i*8+2]) * UInt64(mult2)
        a[i] += UInt64(aa[i*8+3]) * UInt64(mult3)
        a[i] += UInt64(aa[i*8+4]) * UInt64(mult4)
        a[i] += UInt64(aa[i*8+5]) * UInt64(mult5)
        a[i] += UInt64(aa[i*8+6]) * UInt64(mult6)
        a[i] += UInt64(aa[i*8+7]) * UInt64(mult7)
    }
    var b: [UInt64] = Array(repeating: 0, count:5)
    var t: UInt64 = 0
    for i in 0..<24 {
        // Theta
        for x in 0..<5 {
            b[x] = 0
            for y in 0..<5 {
                b[x] ^= a[x + y * 5]
            }
        }
        for x in 0..<5 {
            for y in 0..<5 {
                a[y * 5 + x] ^= b[(x + 4) % 5] ^ rol(x:b[(x + 1) % 5], s: 1)
            }
        }
        // Rho and pi
        t = a[1]
        for x in 0..<24 {
            b[0] = a[Int(pi[x])]
            a[Int(pi[x])] = rol(x:t, s:UInt64(rho[x]))
            t = b[0]
        }
        // Chi
        for y in 0..<5 {
            for x in 0..<5 {
                b[x] = a[y * 5 + x]
            }
            for x in 0..<5 {
                a[y * 5 + x] = b[x] ^ ((~b[(x + 1) % 5]) & b[(x + 2) % 5])
            }
        }
        // Iota
        a[0] ^= RC[i];
        for i in 0..<a.count {
            aa[i * 8] = UInt8(a[i] % 256)
            aa[i * 8 + 1] = UInt8((a[i] / 256) % 256)
            aa[i * 8 + 2] = UInt8((a[i] / UInt64(mult2)) % 256)
            aa[i * 8 + 3] = UInt8((a[i] / UInt64(mult3)) % 256)
            aa[i * 8 + 4] = UInt8((a[i] / UInt64(mult4)) % 256)
            aa[i * 8 + 5] = UInt8((a[i] / UInt64(mult5)) % 256)
            aa[i * 8 + 6] = UInt8((a[i] / UInt64(mult6)) % 256)
            aa[i * 8 + 7] = UInt8((a[i] / UInt64(mult7)) % 256)
        }
    }
}
/** The sponge-based hash construction. **/
func hash(result: inout [UInt8], input: [UInt8], rate: Int, delim: UInt8) -> Bool {
    if (rate >= Plen) {
        return false
    }
    var a : [UInt8] = Array(repeating: 0, count: 200)
    // Absorb input.
    // Fold P*F over the full blocks of an input.
    var j = 0
    while (j + rate <= input.count) {
        for i in 0..<rate {
            a[i] ^= input[i+j]
        }
        keccakf(aa:&a)
        j += rate
    }
    // end FoldP
    // Xor in the DS and pad frame.
    a[input.count - j] ^= delim;
    a[rate - 1] ^= 0x80;
    // Xor in the last block.
    for i in 0..<input.count % rate {
        a[i] ^= input[i+j]
    }
    // Apply P
    keccakf(aa:&a);
    // Squeeze output.
    // Fold P*F over the full blocks of an input.
    j = 0
    while (j + rate <= result.count) {
        for i in 0..<rate {
            result[i] = a[i+j]
        }
        keccakf(aa:&a)
        j += rate
    }
    // end FoldP
    for i in 0..<result.count % rate {
        result[i] = a[i+j]
    }
    return true
}
// result[32]
public func keccak256(result: inout [UInt8], input: [UInt8]) -> Bool {
    let bits = 256
    if (result.count > bits/8) {
        return false
    }
    return hash(result : &result, input: input, rate: Plen - bits/4, delim: 0x01)
}

public func keccak256(_ input: [UInt8]) -> [UInt8] {
    var res: [UInt8] = Array(repeating: 0, count: 32)
    _ = keccak256(result: &res, input: input)
    return res
}
