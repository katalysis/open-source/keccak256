import XCTest
@testable import keccak256

class keccak256Tests: XCTestCase {
    func testkeccak256() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(keccak256(Array("Hello, World!".utf8)).reduce("", { (r, i) -> String in
            return r + String(format: "%02X", i)
        }), "acaf3289d7b601cbd114fb36c4d29c85bbfd5e133f14cb355c3fd8d99367964f".uppercased())
    }

    func testkeccak256EmptyString() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(keccak256(Array("".utf8)).reduce("", { (r, i) -> String in
            return r + String(format: "%02X", i)
        }), "c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470".uppercased())
    }
    
    static var allTests = [
        ("testkeccak256", testkeccak256),
        ("testkeccak256EmptyString", testkeccak256EmptyString),
    ]
}
