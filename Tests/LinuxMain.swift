import XCTest
@testable import keccak256Tests

XCTMain([
    testCase(keccak256Tests.allTests),
])
